/*This file is part of Medito App.

Medito App is free software: you can redistribute it and/or modify
it under the terms of the Affero GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Medito App is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
Affero GNU General Public License for more details.

You should have received a copy of the Affero GNU General Public License
along with Medito App. If not, see <https://www.gnu.org/licenses/>.*/

import 'package:flutter/material.dart';

class MeditoColors {
  static const lightColor = Color(0xffebe7e4);
  static const red = Colors.red;
  static const lightTextColor = Color(0xffa7aab1);
  static const lightColorLine = Color(0xff595F65);
  static const lightColorTrans = Color(0x30595F65);
  static var darkColor = Color(0xff343b43);
  static var darkBGColor = Color(0xff22282D);
  static var lightBlack = Color(0xb2000000);
  static var almostBlack = Color(0xEE000000);
}